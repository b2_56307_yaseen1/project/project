import pprint
from requests_html import HTMLSession
import requests
from bs4 import BeautifulSoup
import re
import pandas as pd

url = 'https://www.amazon.in/American-Tourister-AMT-SCH-02/dp/B07CJCGM1M/?_encoding=UTF8&pd_rd_w=b4ELz&' \
      'pf_rd_p=6486d470-f2a3-47ef-9df5-cd76607dc002&pf_rd_r=F1BTAVBCQCBH6MYQ7PB3&' \
      'pd_rd_r=e16fe261-d310-4f6e-ad62-283b27de8a97&pd_rd_wg=Go0L0&ref_=pd_gw_ci_mcx_mi&th=1'
review_list = []
url = re.search("(.*\/)", url)
url = url.group(0).split("/")
url[4] = "product-reviews"
url = "/".join(url)
print(url+"ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews")

# https://www.amazon.in/American-Tourister-AMT-SCH-02/product-reviews/B07CJCGM1M/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews


def get_soup(url):
    r = requests.get(url=url, params={'wait': 2})
    soup = BeautifulSoup(r.text, 'html.parser')
    return soup


def get_reviews(soup):
    reviews = soup.find_all('div', {'data-hook': 'review'})
    try:
        for item in reviews:
            review = {
                'title': item.find('a', {'data-hook': 'review-title'}).text.strip(),
                'rating': float(item.find('i', {'data-hook': 'review-star-rating'}).text.split(' ')[0].strip()),
                'review_text': item.find('span', {'data-hook': 'review-body'}).text.replace('\n', ' ').strip()
            }
            review_list.append(review)
    except:
        pass


# increase upper limit as required
for x in range(1, 51):
    soup = get_soup(
        f'https://www.amazon.in/Sparx-SC0496G_BKBY0009-Mens-Sc0496g-Sneakers/product-reviews/B083GV2KSF/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews&pageNumber={x}')
    get_reviews(soup)
    print(len(review_list))
    # check if the next page button is disabled
    if not soup.find('li', {'class': 'a-disabled a-last'}):
        pass
    else:
        break

df = pd.DataFrame(review_list)
# print(df[['review_text', 'rating']].head())
df.to_csv('./scraped-reviews.csv')