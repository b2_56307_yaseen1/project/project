import os
from flask import Flask, request, redirect, url_for, send_from_directory, render_template
from werkzeug.utils import secure_filename
import numpy as np
import pickle
import requests
from bs4 import BeautifulSoup
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
import re

# nltk.download('WordNetLemmatizer')
# nltk.download('word_tokenize')
nltk.download('omw-1.4')

file = open("./models/nb_model.pkl", 'rb')
model = pickle.load(file)
file.close()

file2 = open("./models/tfidf_model_ngram.pkl", 'rb')
vectorizer = pickle.load(file2)
file2.close()

app = Flask(__name__)


@app.route("/", methods=['GET'])
def root():
    return """
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h1>Product Review Sentiment Analyzer</h1>
            <form method="POST" action="/predict">
                Amazon Product Link <input name="review_link" type="text" placeholder="Enter amazon product link">
                <button>Analyze</button>
            </form>
        """


# https://www.amazon.in/American-Tourister-AMT-SCH-02/dp/B07CJCGM1M/?_encoding=UTF8&pd_rd_w=b4ELz&
# pf_rd_p=6486d470-f2a3-47ef-9df5-cd76607dc002&pf_rd_r=F1BTAVBCQCBH6MYQ7PB3&pd_rd_r=e16fe261-d310-4f6e-ad62-283b27de8a97&pd_rd_wg=Go0L0&ref_=pd_gw_ci_mcx_mi&th=1

# https://www.amazon.in/American-Tourister-AMT-SCH-02/product-reviews/B07CJCGM1M/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews


@app.route("/predict", methods=['POST'])
def sentiment_analyzer():
    HEADERS = ({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64)'
                              'AppleWebKit / 537.36(KHTML, like Gecko)'
                              'Chrome / 44.0.2403.157'
                              'Safari / 537.36',
                'Accept-Language': 'en-US, en;q=0.5'})

    review_list = []
    sentiment_list = []
    product_url = request.form.get("review_link")
    product_url = re.search("(.*\/)", product_url)
    product_url = product_url.group(0).split('/')
    product_url = f"http://amazon.in/{product_url[4]}/{product_url[5]}"

    def get_all_reviews_link():
        url = request.form.get("review_link")
        url = re.search("(.*\/)", url)
        url = url.group(0).split("/")
        url[4] = "product-reviews"
        url = "/".join(url)
        return url + "ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews"

    def get_reviews():

        for x in range(1, 51):
            response = requests.get(url=get_all_reviews_link() + f'&pageNumber={x}', params={'wait': 2})
            soup = BeautifulSoup(response.text, 'html.parser')
            reviews = soup.find_all('div', {'data-hook': 'review'})
            try:
                for item in reviews:
                    review = item.find('span', {'data-hook': 'review-body'}).text.replace('\n', ' ').strip()
                    # 'title': item.find('a', {'data-hook': 'review-title'}).text.strip(),
                    # 'rating': float(item.find('i', {'data-hook': 'review-star-rating'}).text.split(' ')[0].strip()),
                    review_list.append(review)
            except:
                pass

            if not soup.find('li', {'class': 'a-disabled a-last'}):
                pass
            else:
                break

    get_reviews()

    for review in review_list:
        # review_text = request.form.get("review_text")
        stop_words = stopwords.words('english')

        input_text = word_tokenize(review.lower())
        input_text = [word for word in input_text if word.isalpha()]
        input_text = [WordNetLemmatizer().lemmatize(word) for word in input_text]
        input_text = [word for word in input_text if word not in stop_words]
        # print(input_text)
        input_text = vectorizer.transform(input_text)

        result = model.predict(input_text)[0]
        sentiment_list.append(result)

        # print(len(sentiment_list))
        # print(sentiment_list)

    product_response = requests.get(url=product_url, params={'wait': 2}, headers=HEADERS)
    product_soup = BeautifulSoup(product_response.text, 'html.parser')
    # print(product_soup)
    title = product_soup.find('span', {'id': 'productTitle'})
    # print(title)

    # print(sentiment_list)
    neg_sents = sentiment_list.count(0)
    pos_sents = sentiment_list.count(1)
    # print("Negative Sentiment Count", sentiment_list.count(0))
    # print("Positive Sentiment Count", sentiment_list.count(1))

    if (pos_sents / len(sentiment_list)) >= 0.7:
        return f"<h1> The sentiments for {title} is generally positive with {pos_sents / len(sentiment_list) * 100}%<h1>"
    elif (neg_sents / len(sentiment_list)) >= 0.7:
        return f"<h1> The sentiments for {title} is generally negative with {neg_sents / len(sentiment_list) * 100}%<h1>"
    else:
        return f"<h1> The sentiments for {title} is mixed with positive review at {pos_sents / len(sentiment_list) * 100}% " \
               f"and negative review at {neg_sents / len(sentiment_list) * 100} </h1>"


app.run(debug=True, port=5001)
