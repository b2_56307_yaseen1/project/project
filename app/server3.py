import os
from flask import Flask, request, redirect, url_for, send_from_directory, render_template
from werkzeug.utils import secure_filename
import numpy as np
import pickle
import requests
from bs4 import BeautifulSoup
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
import re

# nltk.download('WordNetLemmatizer')
# nltk.download('word_tokenize')
nltk.download('omw-1.4')

max_eval_pages = 5
max_compare_pages = 3

file = open("./models/nb_model.pkl", 'rb')
model = pickle.load(file)
file.close()

file2 = open("./models/tfidf_model_ngram.pkl", 'rb')
vectorizer = pickle.load(file2)
file2.close()

app = Flask(__name__)


# Root Page (Page1)
@app.route("/", methods=["GET"])
def root():
    return """
        <body style = "background-color:powderblue;">
        <outer "justify-content:center;">
        <center>
        Welcome to
        <h1>Product Evaluator</h1>
        Choose your pick...
        <form action="/evaluator" method="GET">
        <div>
            <button>Analyze a product</button>
        </div>
        </form>
        <form action="/comparator" method="GET">
            <button>Compare 2 products</button>
        </center>
        </form>
        </outer>
        </body>
    """

# Single product Page 2
@app.route("/evaluator", methods = ["GET"])
def evaluate():
    return """
        <body style = "background-color:powderblue;">
        <form action="/evaluator/analysisResult" method="POST">
            Paste Amazon product link <input name="prod_link" type="text" placeholder="Paste link here">
            <button>Analyze Product</button>
        </form>
        </body>
    """


# Single product Page 3
@app.route("/evaluator/analysisResult", methods=["POST"])
def get_all_reviews_evaluator():
    HEADERS = ({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64)'
                              'AppleWebKit / 537.36(KHTML, like Gecko)'
                              'Chrome / 44.0.2403.157'
                              'Safari / 537.36',
                'Accept-Language': 'en-US, en;q=0.5'})

    # review_list = []
    # sentiment_list = []

    # Constructing URL for name extraction
    product_url = request.form.get("prod_link")
    product_url = re.search("(.*\/)", product_url)
    product_url = product_url.group(0).split('/')
    product_url = f"http://amazon.in/{product_url[4]}/{product_url[5]}"

    # Getting input link in Root Page
    prod_link = request.form.get("prod_link")

    # Extracting Base URL
    def get_all_reviews_link():
        base_url = re.search("(.*\/)", prod_link)  # Returns match object
        base_url = base_url.group(0).split('/')
        # 0 returns all matched object, 1 will returned 1st matched object and so on
        # base_url = https , , www.amazon.in, American-Tourister-AMT-SCH-03, dp, B07CGSJNML
        # 0 : https
        # 1 :
        # 2 : www.amazon.in
        # 3 : Product name
        # 4 : dp
        # 5 : asin
        # prod_url = https://www.amazon.in/American-Tourister-AMT-SCH-03/product-reviews/B07CGSJNML/
        #            ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews
        base_url[4] = "product-reviews"

        # Creating Product URL
        return f"http://amazon.in/{base_url[3]}/{base_url[4]}/{base_url[5]}/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews"

    review_list = []
    sentiment_list = []
    def get_reviews():

        for x in range(1, max_eval_pages):
            response = requests.get(url=get_all_reviews_link() + f'&pageNumber={x}', params={'wait': 2})
            soup = BeautifulSoup(response.text, 'html.parser')
            reviews = soup.find_all('div', {'data-hook': 'review'})
            try:
                for item in reviews:
                    review = {
                        'reviews.title': item.find('a', {'data-hook': 'review-title'}).text.strip(),
                        # 'reviews.rating': float(item.find('i', {'data-hook': 'review-star-rating'}).text.split(' ')[0].strip()),
                        'reviews.text': item.find('span', {'data-hook': 'review-body'}).text.strip()
                    }
                    # review = item.find('span', {'data-hook': 'review-body'}).text.replace('\n', ' ').strip()
                    # # 'title': item.find('a', {'data-hook': 'review-title'}).text.strip(),
                    # # 'rating': float(item.find('i', {'data-hook': 'review-star-rating'}).text.split(' ')[0].strip()),
                    review_list.append(review)
            except:
                pass

            # Check if next page button is disabled
            if not soup.find('li', {'class': 'a-disabled a-last'}):
                pass
            else:
                break

    get_reviews()

    # Dataframe Formatting

    df = pd.DataFrame(review_list)
    df['reviews.text'] = df[['reviews.title', 'reviews.text']].agg('. '.join, axis=1) # Combining Columns
    df.drop(labels='reviews.title', axis=1, inplace=True)  # Deleting Old Column

    # Text Processing

    my_stop_words = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll",
                     "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's",
                     'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs',
                     'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am',
                     'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does',
                     'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while',
                     'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during',
                     'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over',
                     'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all',
                     'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'only', 'own',
                     'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'should', "should've", 'now',
                     'd', 'll', 'm', 'o', 're', 've', 'y', 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn',
                     "needn't", 'shan', "shan't", 'shouldn', "shouldn't"]
    lemmatizer = WordNetLemmatizer()
    bag_of_words = []
    for review in df['reviews.text']:  # len(df_nlp['reviews.text'])
        review = nltk.sent_tokenize(str(review))  # Review to Sentence Vector
        review = re.sub('[^a-zA-Z]', ' ', str(review))  # Substitute all non characters with space ' '
        review = review.lower()
        review = review.split()
        review = [lemmatizer.lemmatize(word) for word in review if
                  word not in set(my_stop_words)]  # list of lemmatized words
        #     df_nlp['reviews.text'][i] = ' '.join(review)
        review = ' '.join(review)  # Converting list to string
        bag_of_words.append(review)  # appending string to bag_of_words list


    # Prediction
    x = vectorizer.transform(bag_of_words).toarray()
    sentiment_array = model.predict(x)

    product_response = requests.get(url=product_url, params={'wait': 2}, headers=HEADERS)
    product_soup = BeautifulSoup(product_response.text, 'html.parser')
    # print(product_soup)
    title = product_soup.find('span', {'id': 'productTitle'})
    # print(title)

    # Summarizing
    neg_count = 0
    pos_count = 0
    for elem in sentiment_array:
        if elem == 0:
            neg_count += 1
        else:
            pos_count += 1
    # print(f"In the top n reviews, this product has\n {(pos_count / len(sentiment_array) * 100)}% positive reviews")

    return f"""
        <body style = "background-color:powderblue;">
        <h1>Result of Product Analysis</h1>
        <div>In the top {max_eval_pages*10} reviews, this product has\n {(pos_count / len(sentiment_array) * 100)}% positive reviews<div>
        </body>
    """

# Comparison Page 2
@app.route("/comparator", methods = ["GET"])
def comparator():
    return """
        <body style = "background-color:powderblue;">
        <center>
        <form action="/comparator/comparisonResult" method="POST">
        <div>
            Paste link of 1st product <input name="prod_link1" type="text" placeholder="Paste link here">
        </div>
        <div>
            Paste link of 2nd product <input name="prod_link2" type="text" placeholder="Paste link here">
        </div>
        <div>
            <button>Compare Products</button>
        </div>
        </form>
        </center>
        </body>
    """

# Comparison Page 3
@app.route("/comparator/comparisonResult", methods=["POST"])
def get_all_reviews_comparator():
    HEADERS = ({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64)'
                              'AppleWebKit / 537.36(KHTML, like Gecko)'
                              'Chrome / 44.0.2403.157'
                              'Safari / 537.36',
                'Accept-Language': 'en-US, en;q=0.5'})

    # review_list = []
    # sentiment_list = []

    # Constructing URL for name extraction
    product_url1 = request.form.get("prod_link1")
    product_url2 = request.form.get("prod_link2")
    def name_url(product_url):
        product_url = re.search("(.*\/)", product_url)
        product_url = product_url.group(0).split('/')
        product_url = f"http://amazon.in/{product_url[4]}/{product_url[5]}"
        return product_url
    # url1
    product_url1 = name_url(product_url1)
    # url2
    product_url2 = name_url(product_url2)

    # Getting input link in Root Page
    prod_link1 = request.form.get("prod_link1")
    prod_link2 = request.form.get("prod_link2")

    # Extracting Base URL
    def get_all_reviews_link(prod_link):
        base_url = re.search("[^\/]*\/\/[^\/]*\/[^\/]*\/[^\/]*\/[^\/]*\/", prod_link) # Returns match object
        base_url = base_url.group(0).split('/')
        # 0 returns all matched object, 1 will returned 1st matched object and so on
        # base_url = https , , www.amazon.in, American-Tourister-AMT-SCH-03, dp, B07CGSJNML
        # 0 : https
        # 1 :
        # 2 : www.amazon.in
        # 3 : Product name
        # 4 : dp
        # 5 : asin
        # prod_url = https://www.amazon.in/American-Tourister-AMT-SCH-03/product-reviews/B07CGSJNML/
        #            ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews
        base_url[4] = "product-reviews"

        # Creating Product URL
        return f"http://amazon.in/{base_url[3]}/{base_url[4]}/{base_url[5]}/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews"


    sentiment_list = []
    def get_reviews(prod_link):
        review_list = []
        for x in range(1, max_compare_pages):
            response = requests.get(url=get_all_reviews_link(prod_link) + f'&pageNumber={x}', params={'wait': 2})
            soup = BeautifulSoup(response.text, 'html.parser')
            reviews = soup.find_all('div', {'data-hook': 'review'})
            try:
                for item in reviews:
                    review = {
                        'reviews.title': item.find('a', {'data-hook': 'review-title'}).text.strip(),
                        # 'reviews.rating': float(item.find('i', {'data-hook': 'review-star-rating'}).text.split(' ')[0].strip()),
                        'reviews.text': item.find('span', {'data-hook': 'review-body'}).text.strip()
                    }
                    # review = item.find('span', {'data-hook': 'review-body'}).text.replace('\n', ' ').strip()
                    # # 'title': item.find('a', {'data-hook': 'review-title'}).text.strip(),
                    # # 'rating': float(item.find('i', {'data-hook': 'review-star-rating'}).text.split(' ')[0].strip()),
                    review_list.append(review)
            except:
                pass

            # Check if next page button is disabled
            if not soup.find('li', {'class': 'a-disabled a-last'}):
                pass
            else:
                break
        return review_list

    # Collecting Reviews
    review_list1 = get_reviews(prod_link1)
    review_list2 = get_reviews(prod_link2)

    # Reviews List to Dataframe
    df1 = pd.DataFrame(review_list1)
    df2 = pd.DataFrame(review_list2)

    # Dataframe Formatting
    df1['reviews.text'] = df1[['reviews.title', 'reviews.text']].agg('. '.join, axis=1) # Combining Columns
    df1.drop(labels='reviews.title', axis=1, inplace=True)  # Deleting Old Column

    df2['reviews.text'] = df2[['reviews.title', 'reviews.text']].agg('. '.join, axis=1)  # Combining Columns
    df2.drop(labels='reviews.title', axis=1, inplace=True)  # Deleting Old Column

    # Text Processing

    my_stop_words = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll",
                     "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's",
                     'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs',
                     'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am',
                     'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does',
                     'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while',
                     'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during',
                     'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over',
                     'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all',
                     'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'only', 'own',
                     'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'should', "should've", 'now',
                     'd', 'll', 'm', 'o', 're', 've', 'y', 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn',
                     "needn't", 'shan', "shan't", 'shouldn', "shouldn't"]

    # Creating Bag of Words
    lemmatizer = WordNetLemmatizer()
    def create_bagofwords(df):
        bag_of_words = []
        for review in df['reviews.text']:  # len(df_nlp['reviews.text'])
            review = nltk.sent_tokenize(str(review))  # Review to Sentence Vector
            review = re.sub('[^a-zA-Z]', ' ', str(review))  # Substitute all non characters with space ' '
            review = review.lower()
            review = review.split()
            review = [lemmatizer.lemmatize(word) for word in review if
                      word not in set(my_stop_words)]  # list of lemmatized words
            #     df_nlp['reviews.text'][i] = ' '.join(review)
            review = ' '.join(review)  # Converting list to string
            bag_of_words.append(review)  # appending string to bag_of_words list
        return bag_of_words

    bag_of_words1 = create_bagofwords(df1)
    bag_of_words2 = create_bagofwords(df2)


    # Prediction
    def pred_sentiment_arr(bag_of_words):
        x = vectorizer.transform(bag_of_words).toarray()
        sentiment_array = model.predict(x)
        return sentiment_array

    sentiment_array1 = pred_sentiment_arr(bag_of_words1)
    sentiment_array2 = pred_sentiment_arr(bag_of_words2)

    def get_title(product_url):
        product_response = requests.get(url=product_url, params={'wait': 2}, headers=HEADERS)
        product_soup = BeautifulSoup(product_response.text, 'html.parser')
        # print(product_soup)
        title = product_soup.find('span', {'id': 'productTitle'})
        # print(title)
        return title

    title1 = get_title(product_url1)
    title2 = get_title(product_url2)

    # Summarizing
    def get_sentiment_count(sentiment_array):
        neg_count = 0
        pos_count = 0
        for elem in sentiment_array:
            if elem == 0:
                neg_count += 1
            else:
                pos_count += 1
        return pos_count,neg_count

    pos_count1,neg_count1 = get_sentiment_count(sentiment_array1)
    pos_count2,neg_count2 = get_sentiment_count(sentiment_array2)

    # print(f"In the top n reviews, this product has\n {(pos_count / len(sentiment_array) * 100)}% positive reviews")

    score1 = pos_count1 / len(sentiment_array1) * 100
    score2 = pos_count2 / len(sentiment_array2) * 100
    result = ''
    if score1>score2:
        result = f"The recommended product is <h3>{title1}</h3>"
    elif score1<score2:
        result = f"The recommended product is <h3>{title2}</h3>"
    else:
        result = f"Both the products are equally recommedable"

    return f"""
        <body style = "background-color:powderblue;">
        <h1><u><center>Comparison Analysis Result</center></u></h1>
        <div>As per the top {max_compare_pages*10} reviews,</div>
        <div><h3>{title1}</h3> has <bold>{score1:.2f}%</bold> positive reviews
        <div><h3>{title2}</h3> has <bold>{score2:.2f}%</bold> positive reviews
        <center>
        <div>{result}<div>
        </center>
        </body>
    """

# # Result Page (Page 2)
# @app.route("/result", methods=["POST"])
# def result():
#     print(request.form)
#     review = request.form.get("prod_link") # Getting input link in Root Page
#     pred_value = model.predict(review)  # Predict
#     if pred_value==1:
#         sentiment="Positive"
#     else:
#         sentiment="Negative"
#     return f"""
#         <h1>Result of Product Analysis</h1>
#         <div>The sentence "{request.form}"<div>
#         <div>has a sentiment
#     """

app.run(debug=True)
# app.run(debug=True, port=5001)


# # URL Formatting Operations
# https://www.amazon.in/American-Tourister-AMT-SCH-03/dp/B07CGSJNML/ref=zg-bs_luggage_1/262-4823811-6562866?pd_rd_w=cKXUJ&pf_rd_p=56cde3ad-3235-46d2-8a20-4773248e8b83&pf_rd_r=BRQ8WRRTSRBKJTDJ6YMF&pd_rd_r=e8ea41c7-8e8e-4506-b2e8-7a99e30c5c9c&pd_rd_wg=PbCqO&pd_rd_i=B09FMBCGV4&psc=1
# https://www.amazon.in/American-Tourister-AMT-SCH-03/product-reviews/B07CGSJNML/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews

# https://www.amazon.in/Pigeon-Stovekraft-Plastic-Chopper-Blades/dp/B01LWYDEQ7/ref=cm_cr_arp_d_product_top?ie=UTF8&th=1

# https://www.amazon.in/SAFARI-Ltrs-Casual-Backpack-DAYPACKNEO15CBSEB/dp/B07Q7CNPMV/ref=sr_1_5?crid=1P78J8H62VMM4&keywords=bags&qid=1648907746&s=apparel&smid=AT95IG9ONZD7S&sprefix=bag%2Capparel%2C172&sr=1-5
# https://www.amazon.in/SAFARI-Ltrs-Casual-Backpack-DAYPACKNEO15CBSEB/product-reviews/B07Q7CNPMV/ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews

# Common Append Part
# /product-reviews/ASIN/
# /ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews
# /ref=cm_cr_dp_d_show_all_btm?ie=UTF8&reviewerType=all_reviews

# Working
# (.*\/)
# [^\/]*\/\/[^\/]*\/[^\/]*\/[^\/]*\/[^\/]*\/

# Not Working
# (.*\/(.*\/))
# (.*?&.*?)&.*

# Background colors
# cadetblue
# Cornsilk
# Slategrey
# PaleGoldenRod
#